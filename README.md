# Heritage_Image_Dataset

Here we introduce a heritage image collection dataset we collected from library online open data. The dataset includes: The dataset contains 37,931 valid images with textual information, including tag, description, and location, etc., which are annotated by librarians based on the image content and related research. Based on this dataset, we identify the research issue on image annotation. 

./ Img_ids.txt : Image IDs.
./ Img_tags.txt : Image tags.
./ Img_ descriptions : Image descriptions.
./ Img_places: Image places.

We welcome interested researchers participate in related research problems and there is an on-going data collection process to build a larger and more comprehensive dataset. 

For any further questions, please contact Junjie Zhang (junjie.zhang.avalon@gmail.com). We DO NOT own any these images, the image urls can be obtained via sending a request email to us. Specifically, the researchers interested in the dataset should download and fill up the [Agreement and Disclaimer Form](https://drive.google.com/open?id=1Ej2O_sCtGNyvjn-nW4EJsxqvIEs7TjIT) and send it back to us. We will then email you the instructions to download the images at our discretion.
